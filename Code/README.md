# Visiting detection:

## Phase 1: Preparation

1. (manual process) original groundtruth file (nurses' visiting log): `groundtruth/original_data/SWISKO_rda_2017-08-01_23.30.03.txt`

2. (manual process) userID matching, from the original file to DomoSafety userID, and generate groundtruth file with DomoSafety userId: `groundtruth/original_data/vf_SWISKO_table_correspondance La Source+MedLink+Pryv (06July2017).pdf`

3. (manual process) prepare groundtruth file into format for the code: `{{userId}}_visit_log_gt`, from original file ''. nurse visiting log used as groundtruth: `groundtruth/{{userId}}_visit_log_gt`

4. (manual process) create the following folders: data, detection_data, features, detection_features, models, results.

## Phase 2: Configuration

Prepare user information to be used for training and testing in `configurations.py'.

`ALL_PATIENTS' are the installations used for training the visiting detection model;
`TEST_PATIENTS' are the installations we would like to detect their visiting events.

For each installations the following information are needed: userId, installation token.

## Phase 3: Train visiting detection model

Run `step_4_train_visiting_detector_1csvm.py' to train a One-class SVM model

This function calls the following functions:

`step_1_extract_door_events.py` to generate `data/{{userId}}_doorlog`
This script will try to fetch raw data from Pryv, and extract entrance door closed/open events within the time span where we have groundtruth data

`step_2_prepare_dataset.py` to generate the `data/{{userId}}_tuned_(non)visit_gt_segments` files

`step_3_extract_features.py` to extract features from data and save to `features` folder



## Phase 4: Visiting detection on new/unlabeled data

Run visiting_detection_1csvm.py to detect visiting event from new/unlabeled data

This function calls the following functions:

`step_1_extract_door_events.py` and write the generated door log data to `detection_data/{{userId}}_doorlog`
This script will try to fetch raw data from Pryv, and extract entrance door closed/open events within the time span where we have groundtruth data

`step_2_prepare_dataset.py` to generate the `detection_data/{{userId}}_tuned_(non)visit_gt_segments` files

`step_3_extract_features.py` to extract features from data and save to `detection_features` folder

Write result to `results/'. Each row in the result file indicates a detected visiting event. It contains two columns, 1st column is the starting time of the visit, 2nd column is the ending time of the visit.


## Other codes

We consider four algorithms for visiting detection: 1-class SVM, 2-class SVM, isolation forest, and random forest.  The following codes imeplement experiments on labeled data using leave-one-user-out to train and test the models.

The implementations using 2-class SVM, isolation forest, and random forest need to be further tested.

- `visiting_detection_loo_1csvm.py` : 1-class SVM
- `visiting_detection_loo_2csvm.py` : 2-class SVM
- `visiting_detection_loo_RF.py` : random forest
- `visiting_detection_loo_isolation_forest.py` : isolation forest

