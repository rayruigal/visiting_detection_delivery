ALL_PATIENTS = [
    #
    # ('172293-i', 'cizzdt6z6juen992rwqo6oqre', 'other_information'),  # SWISKO data
    # ('151219-i', 'cj1btq5pvgqbmw62rye4twhi3', 'other_information'),  # https://151219-i.domocare.io/access-info?auth=cj1btq5pvgqbmw62rye4twhi3
    # ('151270-i', 'cj16gqmzb25pmw62r4pvq72yf', 'other_information'),  # have an press/help event
    # ('17163639-i', 'cj16gsc9z25spw62rodz31ttt', 'other_information'),
    #
    # ('17163658-i', 'cj21sxatzhuvgq72r92gayiw9', 'other_information'),  # no data till June
    # ('17163659-i', 'cj278whgg7jgoq72rzkrr17bl', 'other_information'),
    # ('17163661-i', 'cj278wjdm7jhbq72ryeyvvrxj', 'other_information'),
    # ('17163690-i', 'cj46svfiiifhesz2rrgwp7vzv', 'other_information'),
    # ('17163683-i', 'cj3fpun3n02kesc2rza8zu0fl', 'other_information'),
    # # ('17163685-i', 'cj43yhk785rfwsz2rfhsdqdx6', 'other_information'),  # problem, no valid visit
    # ('17163686-i', 'cj43yhl9c5rg8sz2rgrj5jokm', 'other_information'),
    # ('17163687-i', 'cj44gpleu849isz2rjan058z7', 'other_information'),
    # ('17163684-i', 'cj43yhj5l5rfksz2rs19eaut2', 'other_information'),
    # ('17163688-i', 'cj44gplu1849ssz2rfo2tfqv0', 'other_information'),
    # ('151231-i', 'cj4nxxtlfbuztc32rnjnd8ynm', 'other_information'),
    # ('17163691-i', 'cj4nxyu84bv8ac32rqjb6ni99', 'other_information'),
    # ('17163692-i', 'cj4nxyuycbv8oc32rxuinbfz4', 'other_information'),
    # # ('17163700-i', 'cj4nxyx6wbvadc32rucmzqkf0', 'other_information'),  # problem, no valid visit
    # ('17163693-i', 'cj4nxyvnfbv9cc32rs8f810zn', 'other_information'),
    ('17163698-i', 'cj4nxywbrbva3c32rnp1sj6im', 'other_information'),
]

TEST_PATIENTS = [
    #
    # ('172293-i', 'cizzdt6z6juen992rwqo6oqre', 'other_information'),  # SWISKO data
    # ('151219-i', 'cj1btq5pvgqbmw62rye4twhi3', 'other_information'),  # https://151219-i.domocare.io/access-info?auth=cj1btq5pvgqbmw62rye4twhi3
    # ('151270-i', 'cj16gqmzb25pmw62r4pvq72yf', 'other_information'),  # have an press/help event
    # ('17163639-i', 'cj16gsc9z25spw62rodz31ttt', 'other_information'),
    #
    # ('17163658-i', 'cj21sxatzhuvgq72r92gayiw9', 'other_information'),  # no data till June
    # ('17163659-i', 'cj278whgg7jgoq72rzkrr17bl', 'other_information'),
    # ('17163661-i', 'cj278wjdm7jhbq72ryeyvvrxj', 'other_information'),
    # ('17163690-i', 'cj46svfiiifhesz2rrgwp7vzv', 'other_information'),
    # ('17163683-i', 'cj3fpun3n02kesc2rza8zu0fl', 'other_information'),
    # # ('17163685-i', 'cj43yhk785rfwsz2rfhsdqdx6', 'other_information'),  # problem, no valid visit
    # ('17163686-i', 'cj43yhl9c5rg8sz2rgrj5jokm', 'other_information'),
    # ('17163687-i', 'cj44gpleu849isz2rjan058z7', 'other_information'),
    # ('17163684-i', 'cj43yhj5l5rfksz2rs19eaut2', 'other_information'),
    # ('17163688-i', 'cj44gplu1849ssz2rfo2tfqv0', 'other_information'),
    # ('151231-i', 'cj4nxxtlfbuztc32rnjnd8ynm', 'other_information'),
    # ('17163691-i', 'cj4nxyu84bv8ac32rqjb6ni99', 'other_information'),
    # ('17163692-i', 'cj4nxyuycbv8oc32rxuinbfz4', 'other_information'),
    # # ('17163700-i', 'cj4nxyx6wbvadc32rucmzqkf0', 'other_information'),  # problem, no valid visit
    # ('17163693-i', 'cj4nxyvnfbv9cc32rs8f810zn', 'other_information'),
    ('17163698-i', 'cj4nxywbrbva3c32rnp1sj6im', 'other_information'),
]