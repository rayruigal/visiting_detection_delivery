"""
Author: Rui Hu
Contact: ruihu.xidian@gmail.com

Functionality: extract door open/close logs from raw sensor signal

"""
from datetime import datetime, timedelta
from configurations import *
from io_utils import *

def process(userId, domotoken, first_date_str, last_date_str, mode='train'):
    """
    Functionality: load raw sensor data, and extract all door activities between two dates
    :param userId:
    :param domotoken:
    :param first_date: the first date to process, string, e.g. '2017-07-20'
    :param last_date: the last date to process, string, e.g. '2017-07-30'
    :param model: whether it is used in the training function or the detection function
    :return:
    """

    first_date = parse(first_date_str) # convert string to datetime format
    last_date = parse(last_date_str)

    dates = []
    delta = last_date - first_date
    for i in range(delta.days + 1):
        dates.append((first_date + timedelta(days=i)).strftime("%Y-%m-%d"))

    data_folder = 'data/' if mode == 'train' else 'detection_data/'
    door_log = open(data_folder + userId + '_doorlog', 'w')

    for date_index in range(0, len(dates)):
        date = dates[date_index][0:10]

        print date

        # get timestamp of the starting and ending time of a day
        ts = int(time.mktime(datetime.strptime(date, "%Y-%m-%d").timetuple()))
        te = int(time.mktime(((datetime.strptime(date, '%Y-%m-%d') + timedelta(days=1)).timetuple())))

        # read raw sensor data from the pryv
        pryvlink = buildpryvLink(userId, domotoken, ts, te, 1)
        dbuid = retdbuids('https://' + userId + '.domocare.io/streams?auth=' + domotoken + '&tags[]=value')
        rdata = rawEventSignals(pryvlink, dbuid)

        # extract door activities and write to the door log file
        if len(rdata['dn']) > 0:
            for i in range(len(rdata['label_event']) - 1, 0, -1):
                if rdata['label_event'][i].find('door') != -1 and rdata['position'][i].find(
                        'entrance') != -1:  # check if it is a door activity
                    door_log.write(
                        userId.replace('-i', '') + ',' + rdata['label_event'][i] + ',' + date + ',' + time.strftime(
                            "%H:%M:%S", time.localtime(rdata['dn'][i])) + " \n")


if __name__ == "__main__":

    for patient in ALL_PATIENTS:

        userId = patient[0]
        domotoken = patient[1]

        # for training data: readin visit log groundtruth to use the beginning and ending dates in the groundtruth as the first date and last date to process the data
        # for new data: manually give two string dates as the first and last date to process
        gt_fileName = 'groundtruth/' + userId.replace('-i','') + '_visit_log_gt'  # read in nurse visit log
        groundtruth = load_csv(gt_fileName)

        # use the first and last date in the nurse visit log to extract ambient sensor signals
        first_date = groundtruth[0][0]
        last_date = groundtruth[-1][0]

        process(userId, domotoken, first_date, last_date)


