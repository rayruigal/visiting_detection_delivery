import csv
import json
from dateutil.parser import parse
import os
from datetime import datetime
from datetime import timedelta
import time
import numpy as np

def buildpryvLink(installation, domotoken, startTime=[], endTime=[], eventStream=0):
    if eventStream > 0:
        if not startTime or not endTime:
            # startTime = 1467748800 # assign a default value that matches the start of the data collection
            pryvLink = 'https://' + installation + '.domocare.io/events?&streams[]=domosafety&tags[]=value&limit=100&auth=' + domotoken
        else:
            pryvLink = 'https://' + installation + '.domocare.io/events?&streams[]=domosafety&tags[]=value&fromTime=' + str(
                startTime) + '&toTime=' + str(endTime) + '&auth=' + domotoken
    else:
        pryvLink = 'https://' + installation + '.domocare.io/streams?tags[]=value&auth=' + domotoken
    return pryvLink;


def retdbuids(pryvlink):
    # ! /usr/bin/env python

    try:
        # For Python 3.0 and later
        from urllib.request import urlopen
    except ImportError:
        # Fall back to Python 2's urllib2
        from urllib2 import urlopen

    j = urlopen(pryvlink)
    j_obj = json.load(j)
    j2_obj = json.dumps(j_obj['streams'][0]['children'][0])

    j4list = j_obj['streams'][0]['children']
    # read into dictionary with locations and respective dbuid

    dbuid = dict({'locationname': [], 'positionid': [], 'dbuid': []})

    for item in j4list:
        dbuid['locationname'].append(item['name'].replace(' ', '_'))
        dbuid['positionid'].append((item['id'].encode('ascii', 'ignore')).replace(' ', '_'))
        tmplist = []
        # for item2 in item['children'][0]['children']:
        # to include all possible dbuids per location
        #    tmplist.append(item2['id'])
        # dbuid['dbuid'].append(tmplist)
        if (len(item['children']) > 0):
            # tmplist.append(item['children'][1]['children'][0]['id'])
            for item3 in item['children']:  # [1]['children']:   # dbuids from children door
                if (len(item3['children']) > 1):
                    for item2 in item3['children']:
                        tmplist.append(item2['id'])
                else:
                    tmplist.append(item3['children'][0]['id'])
                    # tmplist.append(item['children'][1]['children'][0]['id'])
        dbuid['dbuid'].append(tmplist)
    return dbuid;


def positiondbuid(dbuid, iddb):
    """
    Returns the position id given a dbuid
    """
    position = -1
    ix = 0
    for i in dbuid['dbuid']:
        if iddb in i:
            position = dbuid['locationname'][ix]
            return position;
        ix += 1
    return position;


def rawEventSignals(pryvlink, dbuid):
    """
    functionality: read raw sensor data
    :param pryvlink:
    :param dbuid:
    :return:
    """
    import json
    import sys

    try:
        # For Python 3.0 and later
        from urllib.request import urlopen
    except ImportError:
        # Fall back to Python 2's urllib2
        from urllib2 import urlopen

    rdata = dict({'dn': [], 'duration': [], 'label_event': [], 'position': []})
    j = urlopen(pryvlink)
    j_obj = json.load(j)
    for item in j_obj['events']:
        if (item['type'] == 'electromotive-force/v') or (
            item['type'] == 'motion/off'):  # .encode('ascii','ignore') add more events to be ignored!
            continue
        flag = 0
        if (item['type'] == 'door/open') or (item['type'] == 'door/closed') or (item['type'] == 'bed/in') or (
            item['type'] == 'bed/out'):
            rdata['duration'].append(0)
            flag = 1
        elif item['type'] == 'motion/on':
            rdata['duration'].append(item['duration'])
            flag = 1
        # perhaps add a warning message in case dbuid is not in the list, i.e. position = -1
        tmpPos = positiondbuid(dbuid, item['streamId'].encode('ascii', 'ignore'))
        if flag == 1:
            rdata['dn'].append(item['time'])
            rdata['label_event'].append(item['type'])  # .encode('ascii','ignore')
            rdata['position'].append(tmpPos)  # append('na')
        if tmpPos == -1:  # no position given
            print('time=', str(rdata['dn'][-1]))

    return rdata;


def load_csv(fileName):
    with open(fileName, 'rb') as f:
        reader = csv.reader(f)
        content = []
        for row in reader:
            content_tmp = []
            content_tmp.append(row[1])
            content_tmp.append(int(row[2].split(':')[0]) * 3600 + int(row[2].split(':')[1]) * 60)
            content_tmp.append(int(row[3].split(':')[0]) * 3600 + int(row[3].split(':')[1]) * 60)
            content_tmp.append(float(row[4]))
            content.append(content_tmp)
    return content


def load_door_csv(fileName):

    with open(fileName,'rb') as f:
        reader = csv.reader(f)
        content = []
        for row in reader:
            content_tmp = []
            content_tmp.append(parse(row[2])) # date
            content_tmp.append(row[1]) # door open/closed
            content_tmp.append(int(row[3].split(':')[0]) * 3600 + int(row[3].split(':')[1])*60 + int(row[3].split(':')[2]))  # hh:mm:ss
            # content_tmp.append(int(row[3].split(':')[0])*60 + int(row[3].split(':')[1]) + int(row[3].split(':')[2])/60.0) # hh:mm:ss
            content.append(content_tmp)
    return content


def load_fitbit(userID, date, data_type):  # type can be heart, step, etc

    next_day = datetime.strptime(date,"%Y-%m-%d") + timedelta(days=1)
    time_in_day = int(time.mktime(next_day.timetuple())-time.mktime(datetime.strptime(date,"%Y-%m-%d").timetuple()))

    file_name = '/home/rui/work/SWISKO/data/fitbit/' + userID + '/' + userID + '-' + date + '-activities-steps'
    data_return = np.zeros((time_in_day,2))
    data_return[:,1] = np.arange(time_in_day) + int(time.mktime(datetime.strptime(date,"%Y-%m-%d").timetuple()))
    if os.path.isfile(file_name):
        with open(file_name) as data_file:
            data_all = json.load(data_file)
            if data_all.keys()[0] == "errors":
                print "there is no valid data for " + userID + "on " + date
                flag = 0
            else:
                flag = 1
                data = data_all["activities-" + data_type + "-intraday"]["dataset"]
        if flag:
            for i in range(0, len(data)):
                # time to seconds in a day
                data_return[i*60:(i+1)*60,0] = data[i]["value"]/60.0

    return data_return


def extract_raw_data_between(raw_data, start_time, end_time):
    extracted = {'dn':[], 'duration':[], 'label_event':[], 'position':[]}
    for idx in range(len(raw_data['dn'])):
        # rawdata timestamp is sorted descending
        if raw_data['dn'][idx] > end_time:
            break
        if raw_data['dn'][idx] >= start_time and raw_data['dn'][idx] <= end_time:
            extracted['dn'].append(raw_data['dn'][idx])
            extracted['duration'].append(raw_data['duration'][idx])
            extracted['label_event'].append(raw_data['label_event'][idx])
            extracted['position'].append(raw_data['position'][idx])

            # if len(extracted['dn']) == 0:
            #     print 'crazy', start_time, end_time, end_time - start_time
            # break

        if raw_data['dn'][idx] < start_time and raw_data['dn'][idx]+raw_data['duration'][idx] > start_time:
            extracted['dn'].append(start_time)
            extracted['duration'].append(raw_data['duration'][idx] - (start_time-raw_data['dn'][idx]))
            extracted['label_event'].append(raw_data['label_event'][idx])
            extracted['position'].append(raw_data['position'][idx])

    return extracted


def extract_location_data_between(location, start_time, end_time):
    location_seg = {'location':[], 'duration':[]}
    for idx in range(len(location)-1):
        # location timestamp is sorted descending
        if location[idx][1] == 'unknown':
            continue
        if location[idx][0]>=end_time:
            break
        if location[idx][0]<=start_time:
            if location[idx+1][0] > start_time:
                if location[idx][1] == 'fridge':
                    location[idx][1] = 'kitchen'
                location_seg['location'].append(location[idx][1])
                location_seg['duration'].append(location[idx+1][0]-start_time)
            else:
                continue

        if location[idx][0] > start_time:
            if location[idx][1] == 'fridge':
                location[idx][1] = 'kitchen'
            if len(location_seg['location']) > 0:
                if location[idx-1][1] == location[idx][1]:
                    location_seg['duration'][-1] +=  location[idx+1][0]-location[idx][0]
                else:
                    location_seg['location'].append(location[idx][1])
                    location_seg['duration'].append(min(location[idx + 1][0], end_time) - location[idx][0])
            if len(location_seg['location']) == 0:
                location_seg['location'].append(location[idx][1])
                location_seg['duration'].append(min(location[idx + 1][0],end_time) - location[idx][0])

    return location_seg
