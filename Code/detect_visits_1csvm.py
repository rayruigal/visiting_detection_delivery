import pickle

import utils_extract_door_events
import utils_prepare_dataset
import utils_extract_features
from io_utils import *
from configurations import *



def visiting_detection_1csvm(patient, first_date, last_date):
    userId = patient[0]
    domotoken = patient[1]

    # step 1: extract door open/close activities
    utils_extract_door_events.process(userId, domotoken, first_date, last_date, mode='detection')

    # step 2: pre-processing data, prepare groundtruth etc.,
    utils_prepare_dataset.process(patient[0], mode='detection')  # visualize results by matching fitbit step stream to motion sensor signal

    # step 3. feature extraction
    ts = int(time.mktime(datetime.strptime(first_date, "%Y-%m-%d").timetuple()))
    te = int(time.mktime(datetime.strptime(last_date, "%Y-%m-%d").timetuple()))
    utils_extract_features.run(userId, domotoken, ts, te, mode='detection')

    # step 4. load model
    clf = pickle.load(open("models/1csvm.model", "r"))

    # step 5. load features
    fea = []
    time_data = []
    feature_file = 'features/' + userId + '_fea_visits_new'
    with open(feature_file, 'rb') as f:
        reader = csv.reader(f)

        for row in reader:
            fea.append([float(tmp) for tmp in row][0:len(row) - 4])
            time_data.append([row[-2], row[-1]])

    # step 6. detection
    y = clf.predict(fea)

    # step 7. write results to file
    with open("results/" + userId + "_motion_1csvm_visits", 'w') as f:
        writer = csv.writer(f, delimiter=',')
        for i in range(len(y)):
            if y[i] == 1:
                writer.writerow(time_data[i])


if __name__ == "__main__":

    # give two string dates as the first and last date to process
    first_date = '2017-08-10'
    last_date = '2017-08-20'

    for patient in TEST_PATIENTS:
        visiting_detection_1csvm(patient, first_date, last_date)

