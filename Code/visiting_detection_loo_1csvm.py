# load feature files and use One-class SVM to detect potential visiting activity
# leave one out, use one user as test, the rest as training model


import csv
import numpy as np
from sklearn import svm
from sklearn.cross_validation import train_test_split
from sklearn.cross_validation import cross_val_score
from matplotlib import pyplot as plt

ALL_PATIENTS = [
    ('172293-i', 'cizzdt6z6juen992rwqo6oqre', 'other_information'),  # SWISKO data
    ('151219-i', 'cj1btq5pvgqbmw62rye4twhi3', 'other_information'),
    # https://151219-i.domocare.io/access-info?auth=cj1btq5pvgqbmw62rye4twhi3
    ('151270-i', 'cj16gqmzb25pmw62r4pvq72yf', 'other_information'),  # have an press/help event
    ('17163639-i', 'cj16gsc9z25spw62rodz31ttt', 'other_information'),

    ('17163658-i', 'cj21sxatzhuvgq72r92gayiw9', 'other_information'),  # no data till June
    ('17163659-i', 'cj278whgg7jgoq72rzkrr17bl', 'other_information'),
    ('17163661-i', 'cj278wjdm7jhbq72ryeyvvrxj', 'other_information'),
    ('17163690-i', 'cj46svfiiifhesz2rrgwp7vzv', 'other_information'),
    ('17163683-i', 'cj3fpun3n02kesc2rza8zu0fl', 'other_information'),
    # ('17163685-i', 'cj43yhk785rfwsz2rfhsdqdx6', 'other_information'),  # problem, no valid visit
    ('17163686-i', 'cj43yhl9c5rg8sz2rgrj5jokm', 'other_information'),
    ('17163687-i', 'cj44gpleu849isz2rjan058z7', 'other_information'),
    ('17163684-i', 'cj43yhj5l5rfksz2rs19eaut2', 'other_information'),
    ('17163688-i', 'cj44gplu1849ssz2rfo2tfqv0', 'other_information'),
    ('151231-i', 'cj4nxxtlfbuztc32rnjnd8ynm', 'other_information'), # too few visits, no visit can be detected
    ('17163691-i', 'cj4nxyu84bv8ac32rqjb6ni99', 'other_information'),
    ('17163692-i', 'cj4nxyuycbv8oc32rxuinbfz4', 'other_information'), # too few visits, no visit can be detected
    # ('17163700-i', 'cj4nxyx6wbvadc32rucmzqkf0', 'other_information'),  # problem, no valid visit
    ('17163693-i', 'cj4nxyvnfbv9cc32rs8f810zn', 'other_information'),
    ('17163698-i', 'cj4nxywbrbva3c32rnp1sj6im', 'other_information'),
                ]

## train models

cross_validation_option = 1

# load features from all users
flag_visit = []
flag_non_visit = []
fea_visit = []
fea_non_visit = []
time_visit = []
time_nonvisit = []
for patient_index in range(len(ALL_PATIENTS)):
    userId = ALL_PATIENTS[patient_index][0]

    feature_file = 'features/' + userId + '_fea_visits_new'

    # def load_fea(feature_file):
    with open(feature_file,'rb') as f:
        reader = csv.reader(f)

        for row in reader:
            if row[-3] == '1':
                fea_visit.append([float(tmp) for tmp in row][0:len(row)-4])
                time_visit.append([row[-2],row[-1]])
                flag_visit.append(patient_index)
            if row[-3] == '0':
                fea_non_visit.append([float(tmp) for tmp in row][0:len(row)-4])
                time_nonvisit.append([row[-2], row[-1]])
                flag_non_visit.append(patient_index)

fea_visit = np.asarray(fea_visit)
fea_non_visit = np.asarray(fea_non_visit)

# visiting detection using leave one user out
flag_index_visit = 0
flag_index_nonvisit = 0
for patient_index in range(len(ALL_PATIENTS)):
    userId = ALL_PATIENTS[patient_index][0]
    # fit the model
    # for gamma in np.arange(0,0.00002,0.000001): # selected parameter: 0.00001
    score_train = []
    score_valid = []
    score_test = []

    test_visit_index = [i for i, x in enumerate(flag_visit) if x == patient_index] # find all features (positive visiting events) from this user, to be used as test data, and the rest as training data
    train_visit_index = [i for i, x in enumerate(flag_visit) if x != patient_index]

    test_non_visit_index = [i for i, x in enumerate(flag_non_visit) if x == patient_index] # find all features (unlabeled data) from this user, to be used as test data, and the rest as training data
    train_non_visit_index = [i for i, x in enumerate(flag_non_visit) if x != patient_index]

    fea_visit_train = [fea_visit[i] for i in train_visit_index]
    fea_visit_test = [fea_visit[i] for i in test_visit_index]
    fea_non_visit_train = [fea_non_visit[i] for i in train_non_visit_index]
    fea_non_visit_test = [fea_non_visit[i] for i in test_non_visit_index]

    # gamma_range = np.arange(0.0000001,0.000002,0.0000001)
    gamma_range = np.arange(0.00000001, 0.0000001, 0.00000001) # grid search
    flag = -1
    index_flag = []

    for gamma in gamma_range:
        clf = svm.OneClassSVM(nu=0.15, kernel="rbf", gamma=gamma) # classification model
        flag = flag+1

        # cross validation
        precision_train = []
        precision_valid = []
        precision_test = []
        for i in range(50): # repeat the random split 50 times
            X_train, X_valid, y_train, y_valid = train_test_split(fea_visit_train, np.ones(len(fea_visit_train)), test_size = 0.5) # random split training data to be half as training set, half as validation set

            clf.fit(X_train) # fit training data to the model
            y_pred_train = clf.predict(X_train) # predict label on the training set
            y_pred_valid = clf.predict(X_valid) # predict label on the validation set
            y_pred_test = clf.predict(fea_non_visit_test) # predict label on the test set

            precision_train.append(float(len([item for item in y_pred_train if item > 0])) / len(y_pred_train)) # precision on the training set
            precision_valid.append(float(len([item for item in y_pred_valid if item > 0])) / len(y_pred_valid)) # precision on the validation set
            precision_test.append(float(len([item for item in y_pred_test if item > 0])) / len(y_pred_test)) # precision on the test set
        score_train.append(np.median(precision_train))
        score_valid.append(np.median(precision_valid))
        score_test.append(np.median(precision_test))
        index_flag.append(flag)

    gamma = gamma_range[index_flag[np.argmax(score_valid)]] # select gamma

    print "finished training model for userId: " + userId

    # fit model with the selected parameter, use all training data
    clf = svm.OneClassSVM(nu=0.15, kernel="rbf", gamma=gamma)
    clf.fit(fea_visit_train)

    # predict visiting events on the test data
    y_pred_visit = clf.predict(fea_visit_test)
    y_pred_nonvisit = clf.predict(fea_non_visit_test)

    # compute precision
    precision_visit = float(len([item for item in y_pred_visit if item > 0])) / len(y_pred_visit)
    precision_nonvisit = float(len([item for item in y_pred_nonvisit if item > 0])) / len(y_pred_nonvisit)

    print [userId, len(fea_visit_train), len(fea_visit_test), len(fea_non_visit_train), len(fea_non_visit_test), gamma, precision_visit, precision_nonvisit]

    # write results to file
    with open("results/" + userId + "_motion_1csvm_visits_new_loo", 'w') as f:
        writer = csv.writer(f, delimiter=',')
        for i in range(len(y_pred_visit)):
            if y_pred_visit[i] == 1:
                writer.writerow(time_visit[i+flag_index_visit])

        for i in range(len(y_pred_nonvisit)):
            if y_pred_nonvisit[i] == 1:
                writer.writerow(time_nonvisit[i+flag_index_nonvisit])
    flag_index_visit += len(y_pred_visit)
    flag_index_nonvisit += len(y_pred_nonvisit)


print "Process finished."

    # with open("/home/rui/work/SWISKO/code/visiting_detection/" + userId + "_motion_svm_visits_new", 'w') as f:
    #     writer = csv.writer(f, delimiter=',')
    #     for i in range(len(y_pred_visit)):
    #         if y_pred_visit[i] == 1:
    #             writer.writerow(time_visit[i])
    #
    #     for i in range(len(y_pred_nonvisit)):
    #         if y_pred_nonvisit[i] == 1:
    #             writer.writerow(time_nonvisit[i])