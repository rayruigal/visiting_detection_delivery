# -*- coding: utf-8 -*-
"""
Created on Tue Jun 13 14:59:49 2017

preparing files for visiting detection

@author: rui
"""

from datetime import timedelta
from io_utils import *
from configurations import *


def write_files(groundtruth, door_data, userId, mode='train'):
    """
    functionality: create files and write to 'data/'. files: segments between door logs; positive visiting event, unlabeled data
    :param groundtruth: nurse visiting log
    :param door_data: door activities
    :param userId:
    :return:
    """

    data_folder = 'data/' if mode == 'train' else 'detection_data/'

    first_date = min(parse(groundtruth[0][0]), door_data[0][0])
    last_date = max(parse(groundtruth[-1][0]), door_data[-1][0])

    dates = []
    delta = last_date - first_date
    for i in range(delta.days+1):
        dates.append((first_date+timedelta(days=i)).strftime("%d-%m-%Y")) # get all days from the first till the last day


    correct_visits = []
    no_visits = []


    # door_data = [(date, open/close, timeofday)] use the time period between ['door/closed' 'door/open'] as a period to detect activities
    periods = [(door_data[i], door_data[i+1]) for i in range(len(door_data)-1) if (door_data[i+1][2] + int(time.mktime(door_data[i+1][0].timetuple())))-(door_data[i][2]+int(time.mktime(door_data[i][0].timetuple())))>=60]
    # periods = [(door_data[i], door_data[i + 2]) for i in range(len(door_data) - 2) if door_data[i][0] == door_data[i + 3][0]] # consider door open to the next door closed (after the following door closedm) as an potential visiting event


    # write door segments to file
    fileName = data_folder + userId + '_door_segments'
    # if not(os.path.exists(fileName)): # check if the file does not exist, then generate file
    door_logs = open(fileName,'w')
    door_logs.write("date door(second) date door(second)\n")
    for item in periods:
        door_logs.writelines("%s\n" % (item[0][0].strftime("%Y-%m-%d") + ' ' + str(item[0][2]) + ' ' + item[1][0].strftime("%Y-%m-%d") + ' ' + str(item[1][2])))

    # for each segment, check whether it is a positive visiting event
    for p in periods:
        is_added = False
        for v in groundtruth:
            if parse(v[0]) == p[0][0] and parse(v[0]) == p[1][0]:
                if _check_visit(v[1], v[2], p[0][2], p[1][2]):
                    if not is_added:
                        correct_visits.append(p)
                        is_added = True
        if not is_added:
            no_visits.append(p)

# use door logs to correct nurses' manually recorded visits and write to file
    fileName = data_folder + userId + '_tuned_visit_gt_segments'
    # if not (os.path.exists(fileName)): # check if the file exists
    tuned_visit_gt_segments = open(fileName, 'w')
    tuned_visit_gt_segments.writelines(["%s\n" % (str(int(time.mktime(p[0][0].timetuple())+p[0][2])) + ' ' + str(int(time.mktime(p[1][0].timetuple()) + p[1][2])))  for p in correct_visits])
    fileName = data_folder + userId + '_tuned_visit_negative_gt_segments'
    # if not (os.path.exists(fileName)):
    tuned_visit_negative_gt_segments = open(data_folder + userId + '_tuned_visit_negative_gt_segments', 'w')
    tuned_visit_negative_gt_segments.writelines(["%s\n" % (str(int(time.mktime(p[0][0].timetuple()) + p[0][2])) + ' ' + str(int(time.mktime(p[1][0].timetuple()) + p[1][2]))) for p in no_visits])



def _check_visit(gt_start, gt_end, start, end):
    """
    functionality: check if a segment between two door activities overlap more than 50% with a nurse visiting log
    :param gt_start:
    :param gt_end:
    :param start:
    :param end:
    :return:
    """
    overlap = (max(gt_start, start), min(gt_end, end))
    if (end-start)*0.5 < overlap[1]-overlap[0]: # if overlap more than 50% we consider it as a positive visiting event, otherwise consider it as an unlabeled data
        return True
    else:
        return False


def process(userId, mode='train'):

    data_folder = 'data/' if mode == 'train' else 'detection_data/'

    # readin visit log groundtruth
    gt_fileName = 'groundtruth/' + userId + '_visit_log_gt'  # read in nurse visit log
    groundtruth = load_csv(gt_fileName)

    door_data = load_door_csv(data_folder + userId + '_doorlog')  # read in door sensor data

    write_files(groundtruth, door_data,userId, mode)  # visualize results by matching fitbit step stream to motion sensor signal


if __name__ == "__main__":
    
    
    for patient in ALL_PATIENTS:
        userId = patient[0].translate(None, '-i')

        process(userId)