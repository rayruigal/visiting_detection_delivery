import utils_extract_door_events
import utils_prepare_dataset
import utils_extract_features

import csv
import numpy as np
from sklearn import svm
from sklearn.cross_validation import train_test_split
from sklearn.cross_validation import cross_val_score
from matplotlib import pyplot as plt
import pickle

from configurations import *


def training():

    # load features from all users
    flag_visit = []
    flag_non_visit = []
    fea_visit = []
    fea_non_visit = []
    time_visit = []
    time_nonvisit = []
    for patient_index in range(len(ALL_PATIENTS)):
        userId = ALL_PATIENTS[patient_index][0]

        feature_file = 'features/' + userId + '_fea_visits_new'

        # def load_fea(feature_file):
        with open(feature_file, 'rb') as f:
            reader = csv.reader(f)

            for row in reader:
                if row[-3] == '1': # '1' indicates it is a labeled positive nurse visiting event
                    fea_visit.append([float(tmp) for tmp in row][0:len(row) - 4])
                    time_visit.append([row[-2], row[-1]])
                    flag_visit.append(patient_index)

    fea_visit = np.asarray(fea_visit)

    score_train = []
    score_valid = []
    gamma_range = np.arange(0.00000001, 0.0000001, 0.00000001)  # grid search
    flag = -1
    index_flag = []

    for gamma in gamma_range:
        clf = svm.OneClassSVM(nu=0.15, kernel="rbf", gamma=gamma)  # classification model
        flag = flag + 1

        # cross validation
        precision_train = []
        precision_valid = []
        for i in range(50):  # repeat the random split 50 times to make sure the randomness of the sampling
            X_train, X_valid, y_train, y_valid = train_test_split(fea_visit, np.ones(len(fea_visit)),test_size=0.5)  # random split training data to be half as training set, half as validation set

            clf.fit(X_train)  # fit training data to the model
            y_pred_train = clf.predict(X_train)  # predict label on the training set
            y_pred_valid = clf.predict(X_valid)  # predict label on the validation set

            precision_train.append(float(len([item for item in y_pred_train if item > 0])) / len(y_pred_train))  # precision on the training set
            precision_valid.append(float(len([item for item in y_pred_valid if item > 0])) / len(y_pred_valid))  # precision on the validation set
        score_train.append(np.median(precision_train))
        score_valid.append(np.median(precision_valid))
        index_flag.append(flag)

    gamma = gamma_range[index_flag[np.argmax(score_valid)]]  # select gamma

    print "finished training model for userId: " + userId

    # fit model with the selected parameter, use all training data
    clf = svm.OneClassSVM(nu=0.15, kernel="rbf", gamma=gamma)
    clf.fit(fea_visit)
    return clf


if __name__ == "__main__":

    # step 1: extract door open/close activities
    for patient in ALL_PATIENTS:

        userId = patient[0]
        domotoken = patient[1]

        # for training data: readin visit log groundtruth to use the beginning and ending dates in the groundtruth as the first date and last date to process the data
        # for new data: manually give two string dates as the first and last date to process
        gt_fileName = 'groundtruth/' + userId + '_visit_log_gt'  # read in nurse visit log
        groundtruth = utils_extract_door_events.load_csv(gt_fileName)

        # use the first and last date in the nurse visit log to extract ambient sensor signals
        first_date = groundtruth[0][0]
        last_date = groundtruth[-1][0]

        utils_extract_door_events.process(userId, domotoken, first_date, last_date)

    # step 2: pre-processing data, prepare groundtruth etc.,
        utils_prepare_dataset.process(patient[0])  # visualize results by matching fitbit step stream to motion sensor signal

    # step 3. feature extraction
        # read visit groundtruth to a list
        gt_file = open("data/" + userId + '_tuned_visit_gt_segments', 'r')
        gt_visits = [map(int, line.split(' ')) for line in gt_file]
        # read non visit ground truth to a list: gt_non_visits = [(start, end)]
        gt_file = open("data/" + userId + '_tuned_visit_negative_gt_segments', 'r')
        gt_non_visits = [map(int, line.split(' ')) for line in gt_file]
        ts = min(gt_visits[0][0], gt_non_visits[0][0])
        te = max(gt_visits[-1][1], gt_non_visits[-1][1])
        utils_extract_features.run(userId, domotoken, ts, te)

    # step 4. train the model
    clf = training()
    pickle.dump(clf, open("models/1csvm.model", "wb"))
