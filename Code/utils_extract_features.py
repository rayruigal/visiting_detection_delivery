"""
functionality: extract features
@author: rui

"""
from datetime import datetime,timedelta
from io_utils import *
from configurations import *


def build_single_feature_vector_dataset(raw_data, steps, samples, label):
    """
    functionality: extract features
    :param raw_data:
    :param steps:
    :param samples: segmentes
    :param label:
    :return:
    """

    # extract location information
    location = []
    location.append([raw_data['dn'][0], raw_data['position'][0]])
    for i in range(1, len(raw_data['label_event'])):
        if raw_data['position'][i] == raw_data['position'][i - 1]:
            continue
        location.append([raw_data['dn'][i], raw_data['position'][i]])

    positions = ['bathroom', 'bedroom', 'entrance', 'kitchen', 'living_room', 'toilet']
    dataset = []
    steps_index = 0
    # extract feature for each segment
    for sample in samples:

        start_time = sample[0]
        end_time = sample[1]

        data = extract_raw_data_between(raw_data, start_time, end_time)
        # data = get_raw_data_from_pryv(userId, domotoken, start_time, end_time)
        features = []
        pos_data = {}
        for idx in range(len(data['dn'])):
            pos_data[data['position'][idx]] = pos_data.get(data['position'][idx],0)+ data['duration'][idx] # total motion per location

        for pos in positions:
            features.append(pos_data.get(pos, 0))

        location_segs = extract_location_data_between(location, start_time, end_time)

        pos_data = {}
        for idx in range(len(location_segs['location'])):
            pos_data[location_segs['location'][idx]] = pos_data.get(location_segs['location'][idx], 0) + location_segs['duration'][idx] # total duration per location

        for pos in positions:
            features.append(pos_data.get(pos, 0))

        features.append(end_time - start_time) # segment total duration
        features.append(sum(features[0:6])) # segment total motion
        features.append(len(location_segs['location'])) # number of location transitions within this segment
        features.append(int(datetime.fromtimestamp(start_time).strftime("%H"))) # segment starting time in hours
        steps_tmp = 0
        for i in range(steps_index, len(steps)):
            if steps[i][1]<start_time:
                continue
            if steps[i][1]>=start_time and steps[i][1]<=end_time:
                steps_tmp += steps[i][0]
            else:
                break
        steps_index = i
        features.append(float(steps_tmp)) # total number of steps
        features.append(1 if label else 0)
        features.append(start_time)
        features.append(end_time)

        dataset.append(features)

    return dataset


def run(userId,domotoken,ts,te, mode='train'):
    """
    functionality: extract features for each segment
    :param userId:
    :param domotoken:
    :return:
    """
    data_folder = 'data/' if mode == 'train' else 'detection_data/'
    feature_folder = 'features/' if mode == 'train' else 'detection_features/'

    # step 1: read visit ground truth to a list: gt_visits = [(start, end)]
    gt_file = open(data_folder + userId + '_tuned_visit_gt_segments','r')
    gt_visits = [map(int, line.split(' ')) for line in gt_file]

    # step 2: read non visit ground truth to a list: gt_non_visits = [(start, end)]
    gt_file = open(data_folder + userId + '_tuned_visit_negative_gt_segments','r')
    gt_non_visits = [map(int, line.split(' ')) for line in gt_file]

    steps = []
    # step 3.a read Fitbit steps data
    start_date = datetime.fromtimestamp(ts).strftime('%Y-%m-%d')
    end_date = datetime.fromtimestamp(te).strftime('%Y-%m-%d')
    delta = (datetime.strptime(end_date, "%Y-%m-%d") - datetime.strptime(start_date, "%Y-%m-%d")).days +1
    for n in range(delta):
        date = (datetime.strptime(start_date,"%Y-%m-%d") + timedelta(days=n)).strftime("%Y-%m-%d")
        print date
        steps.extend((load_fitbit(userId, date, "steps")).tolist())

    print "finished loading fitbit data"

    # step 3.b: get all raw_data from min(timestamp) to max(timestamp)
    dbuid = retdbuids('https://' + userId + '.domocare.io/streams?auth=' + domotoken)

    raw_data = dict({'dn': [], 'duration': [], 'label_event': [], 'position': []})
    for ts_tmp in range(ts,te,86400*10): # load data from every 10 days, since it was too large to load all data at once
        te_tmp = min(ts_tmp+86400*10,te)
        pryvlink = buildpryvLink(userId,domotoken,ts_tmp,te_tmp,1)
        raw_data_tmp = rawEventSignals(pryvlink, dbuid)
        raw_data['dn'].extend(raw_data_tmp['dn'])
        raw_data['duration'].extend(raw_data_tmp['duration'])
        raw_data['label_event'].extend(raw_data_tmp['label_event'])
        raw_data['position'].extend(raw_data_tmp['position'])

    print "finished loading sensor data"

    index = sorted(range(len(raw_data['dn'])), key=lambda k: raw_data['dn'][k])
    raw_data['dn'] = [raw_data['dn'][i] for i in index]
    raw_data['duration'] = [raw_data['duration'][i] for i in index]
    raw_data['label_event'] = [raw_data['label_event'][i] for i in index]
    raw_data['position'] = [raw_data['position'][i] for i in index]


    # step 4: extract features
    print "start extracting features"
    dataset = build_single_feature_vector_dataset(raw_data, steps, gt_visits,  True)
    dataset.extend(build_single_feature_vector_dataset(raw_data, steps, gt_non_visits,  False))

    # step 5: save to file
    with open(feature_folder + userId + '_fea_visits_new', 'w') as f:
        writer = csv.writer(f, delimiter=',')
        for line in dataset:
            writer.writerow(line)


if __name__ == "__main__":

    for patient in ALL_PATIENTS:

        print patient

        userId = patient[0]
        domotoken = patient[1]

        gt_file = open("data/" + userId.replace('-i', '') + '_tuned_visit_gt_segments', 'r')
        gt_visits = [map(int, line.split(' ')) for line in gt_file]
        # read non visit ground truth to a list: gt_non_visits = [(start, end)]
        gt_file = open("data/" + userId.replace('-i', '') + '_tuned_visit_negative_gt_segments', 'r')
        gt_non_visits = [map(int, line.split(' ')) for line in gt_file]
        ts = min(gt_visits[0][0], gt_non_visits[0][0])
        te = max(gt_visits[-1][1], gt_non_visits[-1][1])

        run(userId, domotoken, ts, te)
