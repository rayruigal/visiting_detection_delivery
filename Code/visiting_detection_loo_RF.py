# load feature files and use Random Forest to detect potential visiting activities
# leave one out, use one user as test, the rest as training model


import csv
import numpy as np
from sklearn import svm
from sklearn.cross_validation import train_test_split
from sklearn.cross_validation import cross_val_score
from matplotlib import pyplot as plt
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report

ALL_PATIENTS = [
    ('172293-i', 'cizzdt6z6juen992rwqo6oqre', 'other_information'),  # SWISKO data
    ('151219-i', 'cj1btq5pvgqbmw62rye4twhi3', 'other_information'),
    # https://151219-i.domocare.io/access-info?auth=cj1btq5pvgqbmw62rye4twhi3
    ('151270-i', 'cj16gqmzb25pmw62r4pvq72yf', 'other_information'),  # have an press/help event
    ('17163639-i', 'cj16gsc9z25spw62rodz31ttt', 'other_information'),

    ('17163658-i', 'cj21sxatzhuvgq72r92gayiw9', 'other_information'),  # no data till June
    ('17163659-i', 'cj278whgg7jgoq72rzkrr17bl', 'other_information'),
    ('17163661-i', 'cj278wjdm7jhbq72ryeyvvrxj', 'other_information'),
    ('17163690-i', 'cj46svfiiifhesz2rrgwp7vzv', 'other_information'),
    ('17163683-i', 'cj3fpun3n02kesc2rza8zu0fl', 'other_information'),
    # ('17163685-i', 'cj43yhk785rfwsz2rfhsdqdx6', 'other_information'),  # problem, no valid visit
    ('17163686-i', 'cj43yhl9c5rg8sz2rgrj5jokm', 'other_information'),
    ('17163687-i', 'cj44gpleu849isz2rjan058z7', 'other_information'),
    ('17163684-i', 'cj43yhj5l5rfksz2rs19eaut2', 'other_information'),
    ('17163688-i', 'cj44gplu1849ssz2rfo2tfqv0', 'other_information'),
    ('151231-i', 'cj4nxxtlfbuztc32rnjnd8ynm', 'other_information'), # too few visits, no visit can be detected
    ('17163691-i', 'cj4nxyu84bv8ac32rqjb6ni99', 'other_information'),
    ('17163692-i', 'cj4nxyuycbv8oc32rxuinbfz4', 'other_information'), # too few visits, no visit can be detected
    # ('17163700-i', 'cj4nxyx6wbvadc32rucmzqkf0', 'other_information'),  # problem, no valid visit
    ('17163693-i', 'cj4nxyvnfbv9cc32rs8f810zn', 'other_information'),
    ('17163698-i', 'cj4nxywbrbva3c32rnp1sj6im', 'other_information'),
                ]

## train models

cross_validation_option = 1

visit_percentage = np.zeros((len(ALL_PATIENTS),len(ALL_PATIENTS)))
non_visit_percentage = np.zeros((len(ALL_PATIENTS),len(ALL_PATIENTS)))

# load features from all users
flag_visit = []
flag_non_visit = []
fea_visit = []
fea_non_visit = []
time_visit = []
time_nonvisit = []
for patient_index in range(len(ALL_PATIENTS)):
    userId = ALL_PATIENTS[patient_index][0]

    feature_file = 'features/' + userId + '_fea_visits_new'

    # def load_fea(feature_file):
    with open(feature_file,'rb') as f:
        reader = csv.reader(f)

        for row in reader:
            if row[-3] == '1':
                fea_visit.append([float(tmp) for tmp in row][0:len(row)-4])
                time_visit.append([row[-2],row[-1]])
                flag_visit.append(patient_index)
            if row[-3] == '0':
                fea_non_visit.append([float(tmp) for tmp in row][0:len(row)-4])
                time_nonvisit.append([row[-2], row[-1]])
                flag_non_visit.append(patient_index)

fea_visit = np.asarray(fea_visit)
fea_non_visit = np.asarray(fea_non_visit)

# visiting detection using leave one user out
flag_index_visit = 0
flag_index_nonvisit = 0
for patient_index in range(len(ALL_PATIENTS)):
    userId = ALL_PATIENTS[patient_index][0]
    # fit the model
    # for gamma in np.arange(0,0.00002,0.000001): # selected parameter: 0.00001
    score_train = []
    score_valid = []

    test_visit_index = [i for i, x in enumerate(flag_visit) if x == patient_index] # find all features from this user, to be used as test data, and the rest as training data
    train_visit_index = [i for i, x in enumerate(flag_visit) if x != patient_index]

    test_non_visit_index = [i for i, x in enumerate(flag_non_visit) if x == patient_index] # find all features from this user, to be used as test data, and the rest as training data
    train_non_visit_index = [i for i, x in enumerate(flag_non_visit) if x != patient_index]

    fea_visit_train = [fea_visit[i] for i in train_visit_index]
    fea_visit_test = [fea_visit[i] for i in test_visit_index]
    fea_non_visit_train = [fea_non_visit[i] for i in train_non_visit_index]
    fea_non_visit_test = [fea_non_visit[i] for i in test_non_visit_index]

    fea_train = fea_non_visit_train + fea_visit_train
    label_train = np.concatenate((np.zeros(len(fea_non_visit_train)), np.ones(len(fea_visit_train))), axis=0)

    rng = np.random.RandomState(1000)
    weight = len(fea_visit_train) / float(len(fea_non_visit_train))
    clf = RandomForestClassifier(n_estimators=10000, max_features=len(fea_train[0]), class_weight={0: weight, 1: 1.0}, random_state=rng, max_depth=4)
    clf.fit(fea_train,label_train)
    y_pred_train = clf.predict(fea_train)
    score_train = clf.score(fea_train, label_train)
    print str(clf.feature_importances_)

    # # grid search parameters
    # grid_parameters = [{'max_depth': [3, 4, 5,
    #                                   6]}]  # , 'max_features': [len(fea_train[0]), len(fea_train[0])-1, len(fea_train[0])-2, len(fea_train[0])-3, len(fea_train[0])-4]}]
    # # scores = ['precision', 'recall']
    # scores = ['precision']
    # weight = len(fea_visit_train) / float(len(fea_non_visit_train))
    # rng = np.random.RandomState(1000)
    # for score in scores:
    #     clf = GridSearchCV(RandomForestClassifier(n_estimators=5000, max_features=len(fea_train[0]), class_weight={0: weight, 1: 1.0}, random_state=rng), grid_parameters, cv=20, scoring='%s_macro' % score)
    #     clf.fit(fea_train, label_train)
    #     means = clf.cv_results_['mean_test_score']
    #     stds = clf.cv_results_['std_test_score']
    #     for mean, std, params in zip(means, stds, clf.cv_results_['params']):
    #         print("%0.3f (+/-%0.03f) for %r" % (mean, std * 2, params))
    #
    # # use parameters of the best result
    # max_depth = clf.cv_results_['params'][np.argmax(means-stds)]['max_depth']
    #
    # clf = RandomForestClassifier(n_estimators=5000, max_features=len(fea_train[0]), class_weight={0: weight, 1: 1.0}, random_state=rng, max_depth=max_depth)
    # clf.fit(fea_train, label_train)
    # y_pred_train = clf.predict(fea_train)
    # print "best parameter for userId: " + str(max_depth)

    fea_test = fea_visit_test + fea_non_visit_test
    label_test = np.concatenate((np.ones(len(fea_visit_test)), np.zeros(len(fea_non_visit_test))), axis=0)
    score_test = clf.score(fea_test,label_test)
    y_pred_test = clf.predict(fea_test)

    # overall precision, considering labeled data as positive examples, and non-labeled data as negative examples
    precision_train = len([item for item in (y_pred_train - label_train) if item == 0]) / float(len(fea_train))
    precision_test = len([item for item in (y_pred_test - label_test) if item == 0]) / float(len(fea_test))

    # percentage of labeled data detected as visit
    pred_label_positive_train = sum(label_train * y_pred_train) / float(sum(label_train))
    pred_label_positive_test = sum(label_test * y_pred_test) / float(sum(label_test))

    # percentage of non-labeled data detected as visit
    pred_unlabel_positive_train = sum((1 - label_train) * y_pred_train) / float(sum(1 - label_train))
    pred_unlabel_positive_test = sum((1 - label_test) * y_pred_test) / float(sum(1 - label_test))

    # score_visit.append(precision_visit)
    # score_nonvisit.append(precision_nonvisit)

    print [userId, len(fea_visit_train), len(fea_visit_test), len(fea_non_visit_train), len(fea_non_visit_test),
           precision_train, precision_test, pred_label_positive_test, pred_unlabel_positive_test]

    index = [item for item in range(len(label_test)) if label_test[item] == 1]
    y_pred_visit = [y_pred_test[i] for i in index]

    index = [item for item in range(len(label_test)) if label_test[item] == 0]
    y_pred_nonvisit = [y_pred_test[i] for i in index]

    with open("features/" + userId + "_motion_svm_visits_new_loo_RF", 'w') as f:
        writer = csv.writer(f, delimiter=',')
        for i in range(len(y_pred_visit)):
            if y_pred_visit[i] == 1:
                writer.writerow(time_visit[i + flag_index_visit])

        for i in range(len(y_pred_nonvisit)):
            if y_pred_nonvisit[i] == 1:
                writer.writerow(time_nonvisit[i + flag_index_nonvisit])
    flag_index_visit += len(y_pred_visit)
    flag_index_nonvisit += len(y_pred_nonvisit)

plt.figure()
plt.imshow(visit_percentage, cmap='hot', interpolation='nearest')
plt.show()

print "Process finished."

    # with open("/home/rui/work/SWISKO/code/visiting_detection/" + userId + "_motion_svm_visits_new", 'w') as f:
    #     writer = csv.writer(f, delimiter=',')
    #     for i in range(len(y_pred_visit)):
    #         if y_pred_visit[i] == 1:
    #             writer.writerow(time_visit[i])
    #
    #     for i in range(len(y_pred_nonvisit)):
    #         if y_pred_nonvisit[i] == 1:
    #             writer.writerow(time_nonvisit[i])